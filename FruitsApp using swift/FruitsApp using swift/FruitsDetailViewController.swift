//
//  FruitsDetailViewController.swift
//  FruitsApp using swift
//
//  Created by OSX on 18/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit

class FruitsDetailViewController: UIViewController {

    @IBOutlet weak var fruitImage: UIImageView!
    @IBOutlet weak var fruitName: UILabel!
    @IBOutlet weak var fruitDescription: UILabel!
    
    var fruitImageName:NSString = ""
    var fruitNameStr:NSString = ""
    var fruitDescriptionStr:NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        fruitImage.image = UIImage(named: fruitImageName as String)
        fruitName.text = fruitNameStr as String
        fruitDescription.text = fruitDescriptionStr as String
        
        self.navigationItem.title = fruitNameStr as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
