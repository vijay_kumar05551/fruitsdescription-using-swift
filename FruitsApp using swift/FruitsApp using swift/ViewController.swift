//
//  ViewController.swift
//  FruitsApp using swift
//
//  Created by OSX on 18/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating {

    @IBOutlet weak var fruitsTableView: UITableView!
    var searchController:UISearchController!
    var fruitNames:NSArray! = []
    var filterArrayResult:NSArray? = []
    var fruitNameImage:NSArray! = []
    var fruitDescription:NSArray! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Here i have implement the search bar functionality
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.delegate = self
     
        // Here i have set the searchbar to the header of tableview
        fruitsTableView.tableHeaderView = searchController.searchBar
        fruitsTableView.addSubview(searchController.searchBar)
        
        // here i have implement the array
        fruitNames = ["Apple", "Mango", "Bananna", "Orange", "Coconut", "Papaya", "Pineapple", "Plum", "Pomegranate", "Wood apple"]
        fruitNameImage = ["apple.png", "mango.png", "bananna.png", "orange.png", "coconut.png", "papaya.png", "pineapple.png", "plum.png", "pomegranate.png", "wood-apple.png"]
        fruitDescription = ["The apple tree is a deciduous tree in the rose family best known for its sweet, pomaceous fruit, the apple. It is cultivated worldwide as a fruit tree, and is the most widely grown species in the genus Malus.", "The mango is a juicy stone fruit belonging to the genus Mangifera, consisting of numerous tropical fruiting trees, cultivated mostly for edible fruit. The majority of these species are found in nature as wild mangoes.", "The banana is an edible fruit – botanically a berry – produced by several kinds of large herbaceous flowering plants in the genus Musa. In some countries, bananas used for cooking may be called plantains, in contrast to dessert bananas.", "The orange is the fruit of the citrus species Citrus × sinensis in the family Rutaceae. The fruit of the Citrus × sinensis is considered a sweet orange, whereas the fruit of the Citrus × aurantium is considered a bitter orange.", "The coconut tree is a member of the family Arecaceae and the only species of the genus Cocos. The term coconut can refer to the whole coconut palm or the seed, or the fruit, which, botanically, is a drupe, not a nut.", "The papaya, papaw, or pawpaw is the plant Carica papaya, one of the 22 accepted species in the genus Carica of the family Caricaceae. It is native to the tropics of the Americas, perhaps from southern Mexico and neighboring Central America", "The pineapple is a tropical plant with an edible multiple fruit consisting of coalesced berries, also called pineapples, and the most economically significant plant in the Bromeliaceae family.", "Plums belong to the Prunus genus of plants and are relatives of the peach, nectarine and almond. They are all considered 'drupes' fruits that have a hard stone pit surrounding their seeds. When plums are dried, they become the fruit we know as prunes.", "The pomegranate, botanical name Punica granatum, is a fruit-bearing deciduous shrub or small tree in the family Lythraceae that grows between 5 and 8 m tall.", "The health benefits of Bael Fruit or Wood Apple include relief from constipation, indigestion, peptic ulcer, piles, respiratory problems, diarrhea, and dysentery.It also boosts the immune system, fights off bacterial and viral infections, reduces inflammation and various inflammatory conditions, prevent cancer, increases milk production for nursing mothers, cures diabetes, increases ocular health, and helps prevent various sexual dysfunctions."]
        
        filterArrayResult = fruitNames
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fruitNames.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier:NSString = "fruitsCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String) ?? UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier as String)
        cell.textLabel?.text = fruitNames.objectAtIndex(indexPath.row) as? String
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let index:NSInteger = indexPath.row
        self.performSegueWithIdentifier("showFruits", sender: index)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let index:NSInteger = sender as! NSInteger
        var fruitViewController:FruitsDetailViewController = FruitsDetailViewController()
        
        if segue.identifier == "showFruits" {
           fruitViewController = segue.destinationViewController as! FruitsDetailViewController
        }
        
        for name in fruitNames {
            
            if name as! String == fruitNames.objectAtIndex(index) as! String {
                
                fruitViewController.fruitImageName = fruitNameImage.objectAtIndex(index) as! String
                fruitViewController.fruitNameStr = fruitNames.objectAtIndex(index) as! String
                fruitViewController.fruitDescriptionStr = fruitDescription.objectAtIndex(index) as! String
                
                break;
            }
        }
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchStr:NSString = searchController.searchBar.text!
        if searchStr.length > 0 {
            let predicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchStr)
            fruitNames = fruitNames.filteredArrayUsingPredicate(predicate)
        }
        else {
            fruitNames = filterArrayResult
        }
        
        fruitsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        fruitNames = filterArrayResult
        fruitsTableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

